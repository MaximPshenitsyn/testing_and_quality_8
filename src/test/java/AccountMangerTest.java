import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;
import root.integrat.AccountManager;
import root.integrat.AccountManagerResponse;
import root.integrat.IServer;
import root.integrat.ServerResponse;

public class AccountMangerTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    IServer server = Mockito.mock(IServer.class);

    AccountManager manager;

    private final Long SESSION_ID = 12345L;
    private final String CORRECT_PWD = "correct";
    private final String INCORRECT_PWD = "incorrect";
    private final String CORRECT_USR = "exists";
    private final String INCORRECT_USR = "not_exists";
	private final Double BALANCE = 50.0D;

    @BeforeEach
    void initManager() {
        manager = new AccountManager() {
            @Override
            protected String makeSecure(String password) {
                return password;
            }
        };
        manager.init(server);
    }

	/**
	 * Login tests
	 */
    @Test
    void callLoginTest1() {
        Mockito.when(server.login(INCORRECT_USR, INCORRECT_PWD))
                .thenReturn(new ServerResponse(ServerResponse.NO_USER_INCORRECT_PASSWORD, null));
        Assertions.assertEquals(manager.callLogin(INCORRECT_USR, INCORRECT_PWD).code,
                AccountManagerResponse.NO_USER_INCORRECT_PASSWORD);
    }

    @Test
    void callLoginTest2() {
        Mockito.when(server.login(CORRECT_USR, INCORRECT_PWD))
                .thenReturn(new ServerResponse(ServerResponse.NO_USER_INCORRECT_PASSWORD, null));
        Assertions.assertEquals(manager.callLogin(CORRECT_USR, INCORRECT_PWD).code,
                AccountManagerResponse.NO_USER_INCORRECT_PASSWORD);
    }

    @Test
    void callLoginTest3() {
        Mockito.when(server.login(CORRECT_USR, CORRECT_PWD))
                .thenReturn(new ServerResponse(ServerResponse.SUCCESS, SESSION_ID));
        AccountManagerResponse resp = manager.callLogin(CORRECT_USR, CORRECT_PWD);
        Assertions.assertEquals(AccountManagerResponse.SUCCEED, resp.code);
        Assertions.assertInstanceOf(Long.class, resp.response);
        Assertions.assertEquals(SESSION_ID, resp.response);
    }

    @Test
    void callLoginTest4() {
        callLoginTest3();
        Assertions.assertEquals(AccountManagerResponse.ALREADY_LOGGED,
                manager.callLogin(CORRECT_USR, CORRECT_PWD).code);
    }

	@Test
	void callLoginTest5() {
		Mockito.when(server.login(CORRECT_USR, CORRECT_PWD))
				.thenReturn(new ServerResponse(ServerResponse.ALREADY_LOGGED, null));
		Assertions.assertEquals(AccountManagerResponse.ALREADY_LOGGED,
				manager.callLogin(CORRECT_USR, CORRECT_PWD).code);
	}

	@Test
	void callLoginTest6() {
		Mockito.when(server.login(CORRECT_USR, CORRECT_PWD))
				.thenReturn(new ServerResponse(ServerResponse.SUCCESS, SESSION_ID.intValue()));
		AccountManagerResponse resp = manager.callLogin(CORRECT_USR, CORRECT_PWD);
		Assertions.assertEquals(AccountManagerResponse.INCORRECT_RESPONSE, resp.code);
	}

	/**
	 * Logout tests
	 */

	// logout of not logged user
	@Test
	void callLogoutTest1() {
		Assertions.assertEquals(AccountManagerResponse.NOT_LOGGED_RESPONSE, manager.callLogout(INCORRECT_USR, SESSION_ID));
	}

	// correct logout of one user
	// checks he is logged out after
	@Test
	void callLogoutTest2() {
		Mockito.when(server.logout(SESSION_ID))
						.thenReturn(new ServerResponse(ServerResponse.SUCCESS, SESSION_ID));
		callLoginTest3();
		Assertions.assertEquals(AccountManagerResponse.SUCCEED, manager.callLogout(CORRECT_USR, SESSION_ID).code);
		// second call
		Assertions.assertEquals(AccountManagerResponse.NOT_LOGGED_RESPONSE, manager.callLogout(CORRECT_USR, SESSION_ID));
	}

	// NOT LOGGED server response
	@Test
	void callLogoutTest3() {
		Mockito.when(server.logout(SESSION_ID))
				.thenReturn(new ServerResponse(ServerResponse.NOT_LOGGED, null));
		callLoginTest3();
		Assertions.assertEquals(AccountManagerResponse.NOT_LOGGED_RESPONSE, manager.callLogout(CORRECT_USR, SESSION_ID));
	}

	// INCORRECT RESPONSE server response
	@Test
	void callLogoutTest4() {
		Mockito.when(server.logout(SESSION_ID))
				.thenReturn(new ServerResponse(ServerResponse.UNDEFINED_ERROR, null));
		callLoginTest3();
		Assertions.assertEquals(AccountManagerResponse.INCORRECT_RESPONSE, manager.callLogout(CORRECT_USR,
				SESSION_ID).code);
	}

	/**
	 * get balance tests
	 */

	// user is not logged
	@Test
	void getBalanceTest1() {
		Assertions.assertEquals(AccountManagerResponse.NOT_LOGGED_RESPONSE, manager.getBalance(CORRECT_USR, SESSION_ID));
	}

	// session and user session codes
	@Test
	void getBalanceTest2() {
		callLoginTest3();
		Assertions.assertEquals(AccountManagerResponse.INCORRECT_SESSION_RESPONSE, manager.getBalance(CORRECT_USR,
				SESSION_ID + 1));
	}

	// success server response
	@Test
	void getBalanceTest3() {
		Mockito.when(server.getBalance(SESSION_ID))
						.thenReturn(new ServerResponse(ServerResponse.SUCCESS, BALANCE));
		callLoginTest3();
		AccountManagerResponse resp = manager.getBalance(CORRECT_USR, SESSION_ID);
		Assertions.assertEquals(AccountManagerResponse.SUCCEED, resp.code);
		Assertions.assertEquals(BALANCE, resp.response);
	}

	// success server response but wrong balance type
	@Test
	void getBalanceTest4() {
		Mockito.when(server.getBalance(SESSION_ID))
				.thenReturn(new ServerResponse(ServerResponse.SUCCESS, BALANCE.intValue()));
		callLoginTest3();
		AccountManagerResponse resp = manager.getBalance(CORRECT_USR, SESSION_ID);
		Assertions.assertEquals(AccountManagerResponse.INCORRECT_RESPONSE, resp.code);
	}

	// not logged server response
	@Test
	void getBalanceTest5() {
		Mockito.when(server.getBalance(SESSION_ID))
				.thenReturn(new ServerResponse(ServerResponse.NOT_LOGGED, null));
		callLoginTest3();
		AccountManagerResponse resp = manager.getBalance(CORRECT_USR, SESSION_ID);
		Assertions.assertEquals(AccountManagerResponse.NOT_LOGGED_RESPONSE, resp);
	}

	/**
	 * deposit tests
	 */

	// not logged test
	@Test
	void depositTest1() {
		Assertions.assertEquals(AccountManagerResponse.NOT_LOGGED_RESPONSE, manager.deposit(CORRECT_USR, SESSION_ID,
				BALANCE));
	}

	// session and user session codes
	@Test
	void depositTest2() {
		callLoginTest3();
		Assertions.assertEquals(AccountManagerResponse.INCORRECT_SESSION_RESPONSE, manager.deposit(CORRECT_USR,
				SESSION_ID + 1, BALANCE));
	}

	// success server response
	@Test
	void depositTest3() {
		Mockito.when(server.deposit(SESSION_ID, BALANCE))
						.thenReturn(new ServerResponse(ServerResponse.SUCCESS, BALANCE));
		callLoginTest3();
		AccountManagerResponse resp = manager.deposit(CORRECT_USR, SESSION_ID, BALANCE);
		Assertions.assertEquals(AccountManagerResponse.SUCCEED, resp.code);
		Assertions.assertEquals(BALANCE, resp.response);
	}

	// success server response but wrong return type
	@Test
	void depositTest4() {
		Mockito.when(server.deposit(SESSION_ID, BALANCE))
				.thenReturn(new ServerResponse(ServerResponse.SUCCESS, BALANCE.intValue()));
		callLoginTest3();
		AccountManagerResponse resp = manager.deposit(CORRECT_USR, SESSION_ID, BALANCE);
		Assertions.assertEquals(AccountManagerResponse.INCORRECT_RESPONSE, resp.code);
	}

	// not logged on server
	@Test
	void depositTest5() {
		Mockito.when(server.deposit(SESSION_ID, BALANCE))
				.thenReturn(new ServerResponse(ServerResponse.NOT_LOGGED, null));
		callLoginTest3();
		AccountManagerResponse resp = manager.deposit(CORRECT_USR, SESSION_ID, BALANCE);
		Assertions.assertEquals(AccountManagerResponse.NOT_LOGGED_RESPONSE, resp);
	}

	/**
	 * withdraw tests
	 */

	// not logged test
	@Test
	void withDrawTest1() {
		Assertions.assertEquals(AccountManagerResponse.NOT_LOGGED_RESPONSE, manager.withdraw(CORRECT_USR, SESSION_ID,
				BALANCE));
	}

	// session and user session codes
	@Test
	void withDrawTest2() {
		callLoginTest3();
		Assertions.assertEquals(AccountManagerResponse.INCORRECT_SESSION_RESPONSE, manager.withdraw(CORRECT_USR,
				SESSION_ID + 1, BALANCE));
	}

	// success server response
	@Test
	void withDrawTest3() {
		Mockito.when(server.withdraw(SESSION_ID, BALANCE))
				.thenReturn(new ServerResponse(ServerResponse.SUCCESS, BALANCE));
		callLoginTest3();
		AccountManagerResponse resp = manager.withdraw(CORRECT_USR, SESSION_ID, BALANCE);
		Assertions.assertEquals(AccountManagerResponse.SUCCEED, resp.code);
		Assertions.assertEquals(BALANCE, resp.response);
	}

	// no money server response
	@Test
	void withDrawTest4() {
		Mockito.when(server.withdraw(SESSION_ID, BALANCE))
				.thenReturn(new ServerResponse(ServerResponse.NO_MONEY, BALANCE - 1));
		callLoginTest3();
		AccountManagerResponse resp = manager.withdraw(CORRECT_USR, SESSION_ID, BALANCE);
		Assertions.assertEquals(AccountManagerResponse.NO_MONEY, resp.code);
		Assertions.assertEquals(BALANCE - 1, resp.response);
	}
}
