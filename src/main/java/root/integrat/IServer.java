package root.integrat;

public interface IServer {

    ServerResponse login(String userName, String mdPass);

    ServerResponse logout(long id);

    ServerResponse withdraw(long id, double balance);

    ServerResponse deposit(long id, double balance);

    ServerResponse getBalance(long id);

}
